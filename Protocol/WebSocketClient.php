<?php

namespace Library\Protocol;

use \Library\Protocol\BaseClient;

class WebSocketClient extends BaseClient
{
    private $ws_origin;
    private $conn_established;
    private $ws_SecKey;
    private $handshake_step;

    /**
     * Рукопожатие по websocket
     *
     * @param array $user_header - дополнительные заголовки
     * @param string|null $main_header - основной заголовок
     * @param string|null $origin - Origin заголовок
     */
    public function send_handshake(array $user_header = [], string $main_header = null, string $origin = null)
    {
        if (!empty($this->handshakeStep)) {
            return;
        }

        if ($origin) {
            $this->ws_origin = $origin;
        }

        // Get Host.
        $host = $this->getHost() . ':' . $this->getPort();

        // Handshake sec-key.
        $this->ws_SecKey = base64_encode(md5(mt_rand(), true));

        $user_header_str = '';
        if (!empty($user_header)) {
            if (is_array($user_header)) {
                foreach ($user_header as $k => $v) {
                    $user_header_str .= "$k: $v\r\n";
                }
            } else {
                $user_header_str .= $user_header;
            }
        }

        //формировнаие заголовка хэндшейка
        $header = ($main_header ?? "GET / HTTP/1.1") . "\r\n" .
            (!preg_match("/\nHost:/i", $user_header_str) ? "Host: " . $this->getHost() . "\r\n" : '') .
            "Connection: Upgrade\r\n" .
            "Upgrade: websocket\r\n" .
            "Origin: " . ($this->ws_origin ?? "*") . "\r\n" .
            "Sec-WebSocket-Version: 13\r\n" .
            "Sec-WebSocket-Key: " . $this->ws_SecKey . "\r\n" . ($user_header_str . "\r\n" ?? null);

        //дополнение заголовка хэндшейка
        if (!empty($this->ws_ClientProtocol)) {
            $header .= "Sec-WebSocket-Protocol: " . $this->ws_ClientProtocol . "\r\n";
        }

        $response_header = $this->send($header);
        //чтение ответа
        if ($response_header) {
            $re = '/^HTTP\/\d\.\d.*?\\n/';
            $matches = null;
            preg_match($re, $response_header, $matches, 0, 0);

            if ($matches) {
                if (strpos($matches[0], "101") !== false) {
                    $this->conn_established = true;
                }
            }
        }

        $this->handshake_step = 1;
    }

    /**
     * Декодирование websocket пакета
     *
     * @param $data
     * @return bool|string
     */
    public static function decode($data)
    {
        $bytes = $data;
        $dataLength = '';
        $mask = '';
        $coded_data = '';
        $decodedData = '';
        $secondByte = sprintf('%08b', ord($bytes[1]));
        $masked = ($secondByte[0] == '1') ? true : false;
        $dataLength = ($masked === true) ? ord($bytes[1]) & 127 : ord($bytes[1]);

        if ($masked === true) {
            if ($dataLength === 126) {
                $mask = substr($bytes, 4, 4);
                $coded_data = substr($bytes, 8);
            } elseif ($dataLength === 127) {
                $mask = substr($bytes, 10, 4);
                $coded_data = substr($bytes, 14);
            } else {
                $mask = substr($bytes, 2, 4);
                $coded_data = substr($bytes, 6);
            }
            for ($i = 0; $i < strlen($coded_data); $i++) {
                $decodedData .= $coded_data[$i] ^ $mask[$i % 4];
            }
        } else {
            if ($dataLength === 126) {
                $decodedData = substr($bytes, 4);
            } elseif ($dataLength === 127) {
                $decodedData = substr($bytes, 10);
            } else {
                $decodedData = substr($bytes, 2);
            }
        }

        return $decodedData;
    }

    /**
     * Кодирование в websocket пакет
     *
     * @param $payload
     * @param string $type
     * @param bool $masked
     * @return string
     */
    public function encode($payload, $type = 'text', $masked = true)
    {
        $frameHead = array();
        $frame = '';
        $payloadLength = strlen($payload);

        switch ($type) {
            case 'text':
                // first byte indicates FIN, Text-Frame (10000001):
                $frameHead[0] = 129;
                break;

            case 'close':
                // first byte indicates FIN, Close Frame(10001000):
                $frameHead[0] = 136;
                break;

            case 'ping':
                // first byte indicates FIN, Ping frame (10001001):
                $frameHead[0] = 137;
                break;

            case 'pong':
                // first byte indicates FIN, Pong frame (10001010):
                $frameHead[0] = 138;
                break;
        }

        // set mask and payload length (using 1, 3 or 9 bytes)
        if ($payloadLength > 65535) {
            $payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
            $frameHead[1] = ($masked === true) ? 255 : 127;
            for ($i = 0; $i < 8; $i++) {
                $frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
            }

            // most significant bit MUST be 0 (close connection if frame too big)
            if ($frameHead[2] > 127) {
                $this->close(1004);
                return false;
            }
        } elseif ($payloadLength > 125) {
            $payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
            $frameHead[1] = ($masked === true) ? 254 : 126;
            $frameHead[2] = bindec($payloadLengthBin[0]);
            $frameHead[3] = bindec($payloadLengthBin[1]);
        } else {
            $frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
        }

        // convert frame-head to string:
        foreach (array_keys($frameHead) as $i) {
            $frameHead[$i] = chr($frameHead[$i]);
        }

        if ($masked === true) {
            // generate a random mask:
            $mask = array();
            for ($i = 0; $i < 4; $i++) {
                $mask[$i] = chr(rand(0, 255));
            }

            $frameHead = array_merge($frameHead, $mask);
        }
        $frame = implode('', $frameHead);
        // append payload to frame:
        for ($i = 0; $i < $payloadLength; $i++) {
            $frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
        }

        return $frame;
    }

    /**
     * @return int
     */
    public function getHandshakeStep()
    {
        return $this->handshake_step;
    }

    /**
     * @return mixed
     */
    public function getWsOrigin()
    {
        return $this->ws_origin;
    }

    /**
     * @param mixed $ws_origin
     */
    public function setWsOrigin($ws_origin): void
    {
        $this->ws_origin = $ws_origin;
    }

    /**
     * @return bool
     */
    public function getConnEstablished()
    {
        return $this->conn_established;
    }
}
