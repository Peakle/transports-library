<?php

namespace Library\Protocol;

class BaseClient
{
    private $host;
    private $port;
    private $protocol;
    private $socket;
    protected $buffer_length;
    protected $protocols_array = [
        'ws' => 'tcp',
        'wss' => 'tls',
        'ssl' => 'tls',
        'tls' => 'tls',
        'tcp' => 'tcp'
    ];

    /**
     * @param $url - полный урл вебсокета
     * @param int $buffer_length
     */
    public function __construct($url, $buffer_length = 4096)
    {
        $this->buffer_length = $buffer_length;
        $info = parse_url($url);

        if (isset($info['scheme'])) {
            $this->protocol = isset($this->protocols_array[$info['scheme']]) ? $this->protocols_array[$info['scheme']] : "tcp";
        } else {
            $this->protocol = "tcp";
        }

        if (isset($info['port'])) {
            $this->port = $info['port'];
        }

        if (isset($info['host'])) {
            $this->host = $info['host'];
        }

        // обработка такого адреса ws://example.com/ws:443
        if (!isset($info['port']) && isset($info['path'])) {
            $re = '/(?<=:).*\d/';
            preg_match($re, $info['path'], $matches, 0, 0);
            $this->port = $matches[0];
        }
    }

    public function __destruct()
    {
        if (is_resource($this->socket)) {
            stream_socket_shutdown($this->socket, STREAM_SHUT_RDWR);
        }
    }

    public function init($timeout = 4)
    {
        $stream_context = stream_context_create([
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false
            ]
        ]);

        $this->socket = stream_socket_client($this->protocol . "://" . $this->host . ":" . $this->port, $errno, $errstr, $timeout, STREAM_CLIENT_CONNECT, $stream_context);
    }

    public function send($data)
    {
        fwrite($this->getSocket(), $data, $this->buffer_length);
        $response = fread($this->getSocket(), $this->buffer_length);
        return $response;
    }

    public function read()
    {
        $response = fread($this->getSocket(), $this->buffer_length);
        return $response;
    }

    /**
     * @return null\string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return null\int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @return null|string
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * @param $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @param $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @param $protocol
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    /**
     * @return mixed
     */
    public function getSocket()
    {
        return $this->socket;
    }

    /**
     * @param mixed $socket
     */
    public function setSocket($socket): void
    {
        $this->socket = $socket;
    }
}
