<?php

namespace Library\Protocol;

use \Library\Protocol\BaseClient;
use \Exception;

/**
 * rfc1928 , rfc1929
 * Class Socks5Client
 * @package Library\Protocol
 */
class Socks5Client extends BaseClient
{

    private $username;
    private $password;
    private $is_host;
    //используемый метод аутентификации
    private $auth_method;
    //авторизован ли клиент на прокси
    private $auth_established;
    //подключен ли прокси к удаленному хосту
    private $conn_established;


    public function init($username = null, $pass = null, $timeout = 4)
    {
        parent::init($timeout);
        $this->setUsername($username);
        $this->setPassword($pass);
    }

    /**
     * (rfc1928)
     *
     * @param string $host - адрес к которому должен подключиться SOCKS5
     * @param $port - порт к которому должен подключиться SOCKS5
     * @return bool|string
     * @throws \Exception
     */
    public function connect_via_proxy(string $host, $port)
    {
        //инициализация соединения
        if ($this->auth_established) {

            //проверка что будет использоваться ip или dns
            $ip_regx = "/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/";
            if (preg_match($ip_regx, $host, $matches)) {

                $this->is_host = false;
                $type = "\x01";
                if (preg_match("/(\d+)\.(\d+)\.(\d+)\.(\d+)/", $host, $ip_octets)) {
                    $host = pack("C4", $ip_octets[1], $ip_octets[2], $ip_octets[3], $ip_octets[4]);
                }
            } else {
                $this->is_host = true;
                $type = "\x03";
            }

            /*
             * сообщение прокси к чему подключиться удаленному хосту/ip , порту
             */
            $packet = pack('C3', 0x05, 0x01, 0x00);

            //укзание типа подключения dns или ip
            $packet .= $type;

            /*
             * указание длинны названия хоста если подключение по dns
             */
            if ($this->is_host) {
                $packet .= pack('C', mb_strlen($host, 'ASCII'));
            }

            $packet .= $host;

            //указание удаленного порта
            $packet .= pack('n', $port);
            $response_packet = $this->send($packet);

            $response = $this->decode_packet('Cver/Cstatus', substr($response_packet, 0, 2));

            if ($response['status'] == 0) {
                $this->conn_established = true;
            } else {
                throw new Exception($this->parseError($response['status']));
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function auth()
    {
        //запрос поддерживаемых методов авторизации
        $init_packet = pack('C4', 0x05, 0x02, 0x00, 0x02);

        $auth_methods_packet = $this->send($init_packet);
        if ($auth_methods_packet) {
            $auth_methods = $this->decode_packet("Cver/Cmethod", $auth_methods_packet);
            if ($auth_methods['ver'] == 5 && $auth_methods['method'] == 2) {
                $auth_method = "pass";
            } elseif ($auth_methods['ver'] == 5 && $auth_methods['method'] == 0) {
                $auth_method = "noauth";
                $this->auth_established = true;
            } else {
                throw new \Exception('неподдерживаемый способ авторизации');
            }
        } else {
            throw new \Exception('прокси не отвечает на запрос инициализации авторизации');
        }

        //проведение авторизации по паролю
        if ($auth_method == "pass") {

            /*
             * создание пакета для авторизации по паролю
             */
            $ulen = strlen($this->username);
            $plen = strlen($this->password);
            $auth_packet = pack('C2', 0x01, $ulen) . $this->username . pack('C', $plen) . $this->password;
            $auth_response_packet = $this->send($auth_packet);

            if ($auth_response_packet) {

                $auth_response = $this->decode_packet("Cver/Cstatus", $auth_response_packet);

                if ($auth_response['ver'] != 1 || $auth_response ['status'] != 0) {
                    throw new \Exception('неверный логин или пароль');
                }

                $this->auth_established = true;
            }
        }
    }

    /**
     * Обработка ошибок в ответе SOCKS5
     *
     * @param $err_num
     * @return string
     */
    private function parseError($err_num)
    {
        switch ($err_num) {
            case 1:
                $message = "general SOCKS server failure";
                break;
            case 2:
                $message = "connection not allowed by ruleset";
                break;
            case 3:
                $message = "Network unreachable";
                break;
            case 4:
                $message = "Host unreachable";
                break;
            case 5:
                $message = "Connection refused";
                break;
            case 6:
                $message = "TTL expired";
                break;
            case 7:
                $message = "Command not supported";
                break;
            case 8:
                $message = "Address type not supported";
                break;
            case 9:
                $message = "to X'FF' unassigned";
                break;
            default:
                $message = "неизвестная ошибка";
                break;
        }
        return $message;
    }

    /**
     * Декодировать пакет
     *
     * @param string $format
     * @param $packet
     * @return array
     */
    private function decode_packet(string $format, $packet)
    {
        return unpack($format, $packet);
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getConnEstablished()
    {
        return $this->conn_established;
    }

    /**
     * @return mixed
     */
    public function getAuthEstablished()
    {
        return $this->auth_established;
    }
}
