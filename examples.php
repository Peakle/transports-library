<?php

use \Library\Protocol\Socks5Client;
use \Library\Protocol\WebSocketClient;
use \Exception;

/**
 * @author Peakle (23.05)
 *
 * Class examples
 */
class examples
{

    /**
     * Check connection with websocket (over ssl) through socks5 proxy
     *
     * @param array $proxy_data
     * @param string $host
     * @param int $port
     * @param array $headers
     * @param string|null $req_header
     * @param string|null $origin
     * @return bool
     * @throws Exception
     */
    public static function check_websocket_via_proxy(array $proxy_data, string $host = 'echo.websocket.org', int $port = 443, array $headers = [], string $req_header = null, string $origin = null)
    {

        //connect to proxy
        $proxy_socket = new Socks5Client($proxy_data['address']);

        //send tcp-handshake
        $proxy_socket->init($proxy_data['user'], $proxy_data['password']);

        try {
            //auth on proxy
            $proxy_socket->auth();

            // send tcp-handshake to remote address through proxy
            $proxy_socket->connect_via_proxy($host, $port);

        } catch (Exception $exception) {
            throw new Exception("{$exception->getMessage()} {$exception->getFile()} {$exception->getLine()}");
        }

        /*
         * if connection to proxy successfully
         * send websocket handshake
         */
        if ($proxy_socket->getConnEstablished()) {

            try {
                stream_socket_enable_crypto($proxy_socket->getSocket(), true, STREAM_CRYPTO_METHOD_TLS_CLIENT);
            } catch (Exception $exception){
                return false;
            }

            $websocket = new WebSocketClient($host . ":" . $port);
            $websocket->setSocket($proxy_socket->getSocket());
            $websocket->send_handshake($headers, $req_header, $origin);

            if ($websocket->getConnEstablished()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check connection with websocket (over ssl)
     *
     * @param string $host
     * @param int $port
     * @param array $headers
     * @param string|null $req_header
     * @param string|null $origin
     * @return bool
     */
    public static function check_websocket(string $host = 'wss://echo.websocket.org', int $port = 443, array $headers = [], string $req_header = null, string $origin = null)
    {
        $socket = new WebSocketClient($host . ":" . $port);
        $socket->init();
        $socket->send_handshake($headers, $req_header, $origin);

        return $socket->getConnEstablished();
    }

}
